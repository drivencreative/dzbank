TCEFORM {
    pages {
        # remove "Frontend Layout"
        layout.disabled = 1

        backend_layout.disabled = 0
        backend_layout_next_level.disabled = 0

        newUntil.disabled = 1
    }
    tt_content {

    }
}
