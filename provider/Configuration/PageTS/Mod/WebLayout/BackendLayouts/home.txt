#
# BACKENDLAYOUT: HOME
#
mod {
    web_layout {
        BackendLayouts {
            provider_home {
                title = LLL:EXT:provider/Resources/Private/Language/locallang.xlf:backend_layout.home
                config {
                    backend_layout {
                        colCount = 1
                        rowCount = 1
                        rows {
                            1 {
                                columns {
                                    1 {
                                        name = LLL:EXT:provider/Resources/Private/Language/locallang.xlf:backend_layout.column.normal
                                        colPos = 13
                                    }
                                }
                            }
                        }
                    }
                }
                icon = EXT:provider/Resources/Public/Images/BackendLayouts/default.png
            }
        }
    }
}
