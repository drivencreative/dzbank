# Genral Settings
config {
    linkVars = L
    uniqueLinkVars = 1
    sys_language_overlay = 1
    sys_language_mode = content_fallback
}

# German
config {
    htmlTag_langKey = de
    language = de
    locale_all = de-DE
    sys_language_uid = 0
}


# English
[globalVar =   GP:L = 1]
    config {
        htmlTag_langKey = en
        language = en
        locale_all = en-US
        sys_language_uid = 1
    }
[end]
