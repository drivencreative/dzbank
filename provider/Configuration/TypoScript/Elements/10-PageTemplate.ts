page = PAGE
page {
    typeNum = 0
    shortcutIcon = EXT:provider/Resources/Public/Icons/favicon.ico

    10 = FLUIDTEMPLATE
    10 {
        templateName = TEXT
        templateName.stdWrap.cObject = CASE
        templateName.stdWrap.cObject {
            key.data = pagelayout

            pagets__provider_default = TEXT
            pagets__provider_default.value = Default

            pagets__provider_home = TEXT
            pagets__provider_home.value = Home

            inline = TEXT
            inline.value = Inline

            default = TEXT
            default.value = Default
        }


        templateRootPaths {
            0 = EXT:provider/Resources/Private/Templates/Page/
            1 = {$page.fluidtemplate.templateRootPath}
        }
        partialRootPaths {
            0 = EXT:provider/Resources/Private/Partials/Page/
            1 = {$page.fluidtemplate.partialRootPath}
        }
        layoutRootPaths {
            0 = EXT:provider/Resources/Private/Layouts/Page/
            1 = {$page.fluidtemplate.layoutRootPath}
        }
        dataProcessing {
            10 = TYPO3\CMS\Frontend\DataProcessing\FilesProcessor
            10 {
                references.fieldName = media
            }
            20 = TYPO3\CMS\Frontend\DataProcessing\MenuProcessor
            20 {
                levels = 2
                includeSpacer = 1
                as = mainnavigation
            }
        }

        settings {
            # Get the TypoScript constants.
            provider {
                pids {
                    startPage = {$provider.pids.startPage}
                    searchPage = {$provider.pids.searchPage}
                    contactPage = {$provider.pids.contactPage}
                    serviceMenuParent = {$provider.pids.serviceMenuParent}
                    footerMenuParent = {$provider.pids.footerMenuParent}

                }
            }
        }
    }

    meta {
        viewport = {$page.meta.viewport}
        robots = {$page.meta.robots}
        apple-mobile-web-app-capable = {$page.meta.apple-mobile-web-app-capable}
        description = {$page.meta.description}
        description {
            override.field = description
        }
        author = {$page.meta.author}
        author {
            override.field = author
        }
        keywords = {$page.meta.keywords}
        keywords {
            override.field = keywords
        }
        X-UA-Compatible = {$page.meta.compatible}
        X-UA-Compatible {
            attribute = http-equiv
        }

        # OpenGraph Tags
        og:title {
            attribute = property
            field = title
        }
        og:site_name {
            attribute = property
            data = TSFE:tmpl|setup|sitetitle
        }
        og:description = {$page.meta.description}
        og:description {
            attribute = property
            field = description
        }
        og:image {
            attribute = property
            stdWrap.cObject = FILES
            stdWrap.cObject {
                references {
                    data = levelfield:-1, media, slide
                }
                maxItems = 1
                renderObj = COA
                renderObj {
                    10 = IMG_RESOURCE
                    10 {
                        file {
                            import.data = file:current:uid
                            treatIdAsReference = 1
                            width = 1280c
                            height = 720c
                        }
                        stdWrap {
                            typolink {
                                parameter.data = TSFE:lastImgResourceInfo|3
                                returnLast = url
                                forceAbsoluteUrl = 1
                            }
                        }
                    }
                }
            }
        }
    }

    includeCSSLibs {

    }

    includeCSS {
    }

    includeJSLibs {
    }

    includeJS {

    }

}