plugin.tx_felogin_pi1._LOCAL_LANG.default {
    ll_welcome_header = Melden Sie sich an
    ll_welcome_message =
    ll_enter_your_data = Benutzername oder Email-Addresse
    username = Benutzername oder Email-Addresse
    password = Passwort
    ll_forgot_header = Passwort vergessen?
    ll_forgot_reset_message = Bitte geben Sie Ihren Benutzernamen oder Email-Adresse ein. Sie erhalten einen Link per Email mit dem Sie ein neues Passwort erstellen können.
    ll_forgot_header_backToLogin = < Zurück zum Log in
    ll_forgot_reset_message_emailSent = Ihre Anfrage wurde an uns weitergeleitet. Sie erhalten in Kürze ein Email.
    ll_change_password_header = Erstellen Sie ein neues Passwort
    ll_change_password_message =
}