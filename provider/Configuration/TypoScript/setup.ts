config {
    no_cache = 1
    cache_period = 86400
    cache_clearAtMidnight = 1
    sendCacheHeaders = 1
    absRefPrefix = /

    notification_email_encoding = quoted-printable
    notification_email_charset = utf-8
    notification_email_urlmode = all

    intTarget = _self
    linkVars = L(0-1)
    aTagParams = onFocus="blurLink(this)"
    typolinkCheckRootline = 1
    typolinkEnableLinksAcrossDomains = 1
    simulateStaticDocuments = 0

    doctype = html5
    xmlprologue = none

    admPanel = 0

    spamProtectEmailAddresses = 2
    spamProtectEmailAddresses_atSubst = <span class="at" data-replace="@">[at]</span>

    noPageTitle = 2

    inlineStyle2TempFile = 0
    removeDefaultJS = 1
    #concatenateCss = 1
    #concatenateJs = 1
    #minifyCSS = 1
    #minifyJS = 1
    #moveJsFromHeaderToFooter = 1
}

## Language
config {
    language = en
    locale_all = en_EN.UTF-8
    sys_language_uid = 0
    sys_language_isocode = en
    sys_language_mode = content_fallback ; 0
    sys_language_overlay = 1
    htmlTag_langKey = en
    defaultGetVars.L = 0
}

#RealUrl
config {
    absRefPrefix = /
    tx_realurl_enable = 1
}

## Page
page = PAGE
page < temp.mask.page
page.typeNum = 0
# Favicon
page.shortcutIcon = EXT:provider/Resources/Public/favicons/favicon.ico
#page.10 < styles.content.get
page {
    10 {
        file.stdWrap.cObject.default.value = EXT:provider/Resources/Private/Templates/Default.html
        file.stdWrap.cObject.404.value = EXT:provider/Resources/Private/Templates/404.html
        layoutRootPaths {
            10 = EXT:provider/Resources/Private/Layouts
        }
        partialRootPaths {
            10 = EXT:provider/Resources/Private/Partials
        }
    }
    # Meta
    meta {
        language < config.language
        title.field = title
        title.data = page.title
        keywords.field = keywords
        keywords.data = page.keywords
        description.field = description
        description.data = page:description
        author.data = levelfield :-1, author, slide
        date {
            data = page:SYS_LASTCHANGED // page:crdate
            date = Y-m-d
        }
        robots = index,follow
        revisit = 1 day
        viewport = width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no
    }
    includeCSS{
        stylesheet = EXT:provider/Resources/Public/Css/stylesheet.css
        critical = EXT:provider/Resources/Public/Css/critical.css
    }
    includeJSFooter{
        bundle = EXT:provider/Resources/Public/Js/bundle.js
    }
}

# Remove empty p tags
tt_content.stdWrap.dataWrap >

# blockquote remove default styles
tt_content.text.20.parseFunc.externalBlocks.blockquote.callRecursive.tagStdWrap.HTMLparser.tags.blockquote.fixAttrib.style.unset=1
lib.parseFunc_RTE.externalBlocks.blockquote.callRecursive.tagStdWrap.HTMLparser.tags.blockquote.overrideAttribs

# Mask content
lib.content0 < styles.content.get
lib.content0.select.where = colPos=0

# Remove empty p tags
lib.parseFunc_RTE.nonTypoTagStdWrap.encapsLines >

#Remove default wrapping
#tt_content.stdWrap.innerWrap >
tt_content.stdWrap.innerWrap.cObject.default >


lib.fluidContent.layoutRootPaths.100 = EXT:provider/Resources/Private/Layouts/FluidStyledContent/
lib.contentElement.layoutRootPaths.100 = EXT:provider/Resources/Private/Layouts/FluidStyledContent/