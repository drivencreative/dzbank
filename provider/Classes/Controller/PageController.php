<?php
namespace Netfed\Provider\Controller;

/***
 *
 * This file is part of the "provider" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018
 *
 ***/

/**
 * PageController
 */
class PageController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * pageRepository
     *
     * @var \Netfed\Provider\Domain\Repository\PageRepository
     * @inject
     */
    protected $pageRepository = null;

    /**
     * action list
     *
     * @return void
     */
    public function listAction()
    {
        $pages = $this->pageRepository->findAll();
        $this->view->assign('pages', $pages);
    }
}
