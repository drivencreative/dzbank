<?php

/**
 * Created by PhpStorm.
 * User: dradisic
 * Date: 1/9/18
 * Time: 3:34 PM
 */
class NavigationController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * rubricRepository
     *
     * @var \Netfed\Dzbankmagazine\Domain\Repository\RubricRepository
     * @inject
     */
    protected $rubricRepository = null;

    /**
     * themeRepository
     *
     * @var \Netfed\Dzbankmagazine\Domain\Repository\ThemeRepository
     * @inject
     */
    protected $themeRepository = null;

    /**
     * magazineRepository
     *
     * @var \Netfed\Dzbankmagazine\Domain\Repository\MagazineRepository
     * @inject
     */
    protected $magazineRepository = null;

    /**
     * archiveRepository
     *
     * @var \Netfed\Dzbankmagazine\Domain\Repository\ArchiveRepository
     * @inject
     */
    protected $archiveRepository = null;

    /**
     * action menu
     *
     * @return void
     */
    public function menuAction()
    {
        $archives = $this->archiveRepository->findAll();
        $themes = $this->themeRepository->findAll();
        $rubric = $this->rubricRepository->findAll();
        \TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($themes);die;
        $this->view->assign('archives', $archives);
        $this->view->assign('themes', $themes);
        $this->view->assign('rubric', $rubric);
    }
}