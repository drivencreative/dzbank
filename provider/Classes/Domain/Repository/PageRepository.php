<?php
namespace Netfed\Provider\Domain\Repository;

/***
 *
 * This file is part of the "provider" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018
 *
 ***/

/**
 * The repository for Pages
 */
class PageRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{
    }
