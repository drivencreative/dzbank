<?php
namespace Netfed\Dzbankmagazine\Domain\Model;

/***
 *
 * This file is part of the "DZ Bank Magazine" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018
 *
 ***/

/**
 * Article
 */
class Article extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * title
     *
     * @var string
     */
    protected $title = '';

    /**
     * subtitle
     *
     * @var string
     */
    protected $subtitle = '';

    /**
     * author
     *
     * @var string
     */
    protected $author = '';

    /**
     * topImage
     *
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     */
    protected $topImage = null;

    /**
     * image
     *
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     */
    protected $image = null;

    /**
     * hoverImage
     *
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     */
    protected $hoverImage;

    /**
     * relatedArticles
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Netfed\Dzbankmagazine\Domain\Model\Article>
     */
    protected $relatedArticles = null;

    /**
     * rubric
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Netfed\Dzbankmagazine\Domain\Model\Rubric>
     */
    protected $rubric = null;

    /**
     * theme
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Netfed\Dzbankmagazine\Domain\Model\Theme>
     */
    protected $theme = null;

    /**
     * content
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Netfed\Dzbankmagazine\Domain\Model\Content>
     */
    protected $content = '';

    /**
     * magazine
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Netfed\Dzbankmagazine\Domain\Model\Magazine>
     */
    protected $magazine = null;

    /**
     * readers
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Netfed\Dzbankuser\Domain\Model\User>
     */
    protected $readers = null;

    /**
     * top
     *
     * @var bool
     */
    protected $top = null;

    /**
     * editorial
     *
     * @var bool
     */
    protected $editorial = null;

    /**
     * Returns the title
     *
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets the title
     *
     * @param string $title
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Returns the subtitle
     *
     * @return string $subtitle
     */
    public function getSubtitle()
    {
        return $this->subtitle;
    }

    /**
     * Sets the subtitle
     *
     * @param string $subtitle
     * @return void
     */
    public function setSubtitle($subtitle)
    {
        $this->subtitle = $subtitle;
    }

    /**
     * Returns the author
     *
     * @return string $subtitle
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param string $author
     * @return void
     */
    public function setAuthor($author)
    {
        $this->author = $author;
    }
    /**
     * __construct
     */
    public function __construct()
    {
        //Do not remove the next line: It would break the functionality
        $this->initStorageObjects();
    }

    /**
     * Initializes all ObjectStorage properties
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     *
     * @return void
     */
    protected function initStorageObjects()
    {
        $this->relatedArticles = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $this->rubric = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $this->theme = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }

    /**
     * Adds a Article
     *
     * @param \Netfed\Dzbankmagazine\Domain\Model\Article $relatedArticle
     * @return void
     */
    public function addRelatedArticle(\Netfed\Dzbankmagazine\Domain\Model\Article $relatedArticle)
    {
        $this->relatedArticles->attach($relatedArticle);
    }

    /**
     * Removes a Article
     *
     * @param \Netfed\Dzbankmagazine\Domain\Model\Article $relatedArticleToRemove The Article to be removed
     * @return void
     */
    public function removeRelatedArticle(\Netfed\Dzbankmagazine\Domain\Model\Article $relatedArticleToRemove)
    {
        $this->relatedArticles->detach($relatedArticleToRemove);
    }

    /**
     * Returns the relatedArticles
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Netfed\Dzbankmagazine\Domain\Model\Article> $relatedArticles
     */
    public function getRelatedArticles()
    {
        return $this->relatedArticles;
    }

    /**
     * Sets the relatedArticles
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Netfed\Dzbankmagazine\Domain\Model\Article> $relatedArticles
     * @return void
     */
    public function setRelatedArticles(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $relatedArticles)
    {
        $this->relatedArticles = $relatedArticles;
    }

    /**
     * Adds a Rubric
     *
     * @param \Netfed\Dzbankmagazine\Domain\Model\Rubric $rubric
     * @return void
     */
    public function addRubric(\Netfed\Dzbankmagazine\Domain\Model\Rubric $rubric)
    {
        $this->rubric->attach($rubric);
    }

    /**
     * Removes a Rubric
     *
     * @param \Netfed\Dzbankmagazine\Domain\Model\Rubric $rubricToRemove The Rubric to be removed
     * @return void
     */
    public function removeRubric(\Netfed\Dzbankmagazine\Domain\Model\Rubric $rubricToRemove)
    {
        $this->rubric->detach($rubricToRemove);
    }

    /**
     * Returns the rubric
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Netfed\Dzbankmagazine\Domain\Model\Rubric> $rubric
     */
    public function getRubric()
    {
        return $this->rubric;
    }

    /**
     * Sets the rubric
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Netfed\Dzbankmagazine\Domain\Model\Rubric> $rubric
     * @return void
     */
    public function setRubric(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $rubric)
    {
        $this->rubric = $rubric;
    }

    /**
     * Adds a Theme
     *
     * @param \Netfed\Dzbankmagazine\Domain\Model\Theme $theme
     * @return void
     */
    public function addTheme(\Netfed\Dzbankmagazine\Domain\Model\Theme $theme)
    {
        $this->theme->attach($theme);
    }

    /**
     * Removes a Theme
     *
     * @param \Netfed\Dzbankmagazine\Domain\Model\Theme $themeToRemove The Theme to be removed
     * @return void
     */
    public function removeTheme(\Netfed\Dzbankmagazine\Domain\Model\Theme $themeToRemove)
    {
        $this->theme->detach($themeToRemove);
    }

    /**
     * Returns the theme
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Netfed\Dzbankmagazine\Domain\Model\Theme> $theme
     */
    public function getTheme()
    {
        return $this->theme;
    }

    /**
     * Sets the theme
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Netfed\Dzbankmagazine\Domain\Model\Theme> $theme
     * @return void
     */
    public function setTheme(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $theme)
    {
        $this->theme = $theme;
    }

    /**
     * Returns the content
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Netfed\Dzbankmagazine\Domain\Model\Content> $content
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Sets the content
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Netfed\Dzbankmagazine\Domain\Model\Content> $content
     * @return void
     */
    public function setContent(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $content)
    {
        $this->content = $content;
    }

    /**
     * Adds a Magazine
     *
     * @param \Netfed\Dzbankmagazine\Domain\Model\Magazine $magazine
     * @return void
     */
    public function addMagazine(\Netfed\Dzbankmagazine\Domain\Model\Magazine $magazine)
    {
        $this->magazine->attach($magazine);
    }

    /**
     * Removes a Magazine
     *
     * @param \Netfed\Dzbankmagazine\Domain\Model\Magazine $magazineToRemove The Magazine to be removed
     * @return void
     */
    public function removeMagazine(\Netfed\Dzbankmagazine\Domain\Model\Magazine $magazineToRemove)
    {
        $this->magazine->detach($magazineToRemove);
    }

    /**
     * Returns the magazine
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Netfed\Dzbankmagazine\Domain\Model\Magazine> $magazine
     */
    public function getMagazine()
    {
        return $this->magazine;
    }

    /**
     * Sets the magazine
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Netfed\Dzbankmagazine\Domain\Model\Magazine> $magazine
     * @return void
     */
    public function setMagazine(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $magazine)
    {
        $this->magazine = $magazine;
    }

    /**
     * Returns the topImage
     *
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $topImage
     */
    public function getTopImage()
    {
        return $this->topImage;
    }

    /**
     * Sets the topImage
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $topImage
     * @return void
     */
    public function setTopImage(\TYPO3\CMS\Extbase\Domain\Model\FileReference $topImage)
    {
        $this->topImage = $topImage;
    }

    /**
     * Returns the image
     *
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $image
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Sets the image
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $image
     * @return void
     */
    public function setImage(\TYPO3\CMS\Extbase\Domain\Model\FileReference $image)
    {
        $this->image = $image;
    }

    /**
     * Returns the hoverImage
     *
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $hoverImage
     */
    public function getHoverImage()
    {
        return $this->hoverImage;
    }
    /**
     * Sets the hoverImage
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $hoverImage
     * @return void
     */
    public function setHoverImage(\TYPO3\CMS\Extbase\Domain\Model\FileReference $hoverImage)
    {
        $this->hoverImage = $hoverImage;
    }

    /**
     * Returns the Estimated Reading Time
     *
     * @return string $hoverImage
     */
    public function getEstimatedReadingTime()
    {
        $content = \Netfed\Dzbankmagazine\Utility\HelperUtility::recursivelyExtractKey($this->content, 'html');
        if ($content) {
            $content = str_word_count(strip_tags(implode(' ', $content)));
        }

        return ceil($content / 200);
    }

    /**
     * Adds a Readers
     *
     * @param \Netfed\Dzbankuser\Domain\Model\User $readers
     * @return void
     */
    public function addReaders(\Netfed\Dzbankuser\Domain\Model\User $readers)
    {
        $this->readers->attach($readers);
    }

    /**
     * Removes a Readers
     *
     * @param \Netfed\Dzbankuser\Domain\Model\User $readersToRemove The Readers to be removed
     * @return void
     */
    public function removeReaders(\Netfed\Dzbankuser\Domain\Model\User $readersToRemove)
    {
        $this->readers->detach($readersToRemove);
    }

    /**
     * Returns the readers
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Netfed\Dzbankuser\Domain\Model\User> $readers
     */
    public function getReaders()
    {
        return $this->readers;
    }

    /**
     * Sets the readers
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Netfed\Dzbankuser\Domain\Model\User> $readers
     * @return void
     */
    public function setReaders(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $readers)
    {
        $this->readers = $readers;
    }

    /**
     * @return bool
     */
    public function isTop()
    {
        return $this->top;
    }

    /**
     * @return bool
     */
    public function getTop()
    {
        return $this->top;
    }

    /**
     * @param bool $top
     */
    public function setTop($top)
    {
        $this->top = $top;
    }

    /**
     * @return bool
     */
    public function isEditorial()
    {
        return $this->editorial;
    }

    /**
     * @return bool
     */
    public function getEditorial()
    {
        return $this->editorial;
    }

    /**
     * @param bool $editorial
     */
    public function setEditorial($editorial)
    {
        $this->editorial = $editorial;
    }

}
