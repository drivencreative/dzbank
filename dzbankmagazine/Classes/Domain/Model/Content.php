<?php
namespace Netfed\Dzbankmagazine\Domain\Model;

/***
 *
 * This file is part of the "DZ Bank Magazine" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018
 *
 ***/

/**
 * Content
 */
class Content extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    /**
     * uid
     *
     * @var string
     */
    protected $uid = '';

    /**
     * pid
     *
     * @var string
     */
    protected $pid = '';

    /**
     * header
     *
     * @var string
     */
    protected $header = '';

    /**
     * content
     *
     * @var string
     */
    protected $content = '';

    /**
     * sorting
     *
     * @var string
     */
    protected $sorting = '';

    /**
     * contentType
     *
     * @var string
     */
    protected $contentType = '';

    /**
     * Gets the uid
     *
     * @return string $uid
     */
    public function getUid() {
        return $this->uid;
    }
    /**
     * Gets the pid
     *
     * @return string $pid
     */
    public function getPid() {
        return $this->pid;
    }

    /**
     * Returns the header
     *
     * @return string $header
     */
    public function getHeader() {
        return $this->header;
    }

    /**
     * Sets the header
     *
     * @param string $header
     * @return void
     */
    public function setHeader($header) {
        $this->header = $header;
    }

    /**
     * Returns the content
     *
     * @return string $content
     */
    public function getContent() {
        return $this->content;
    }

    /**
     * Sets the content
     *
     * @param string $content
     * @return void
     */
    public function setContent($content) {
        $this->content = $content;
    }

    /**
     * Returns the sorting
     *
     * @return string $sorting
     */
    public function getSorting() {
        return $this->sorting;
    }

    /**
     * Sets the sorting
     *
     * @param string $sorting
     * @return void
     */
    public function setSorting($sorting) {
        $this->sorting = $sorting;
    }

    /**
     * Returns the contentType
     *
     * @return string $contentType
     */
    public function getContentType() {
        return $this->contentType;
    }

    /**
     * Sets the contentType
     *
     * @param string $contentType
     * @return void
     */
    public function setContentType($contentType) {
        $this->contentType = $contentType;
    }

    /**
     * Returns the content
     *
     * @return string $content
     */
    public function getHtml()
    {
        return $GLOBALS['TSFE']->cObj->cObjGetSingle('RECORDS', [
            'tables' => 'tt_content',
            'source' => $this->uid,
            'dontCheckPid' => 1
        ]);
    }

}
