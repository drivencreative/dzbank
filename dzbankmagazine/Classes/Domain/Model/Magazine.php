<?php
namespace Netfed\Dzbankmagazine\Domain\Model;

/***
 *
 * This file is part of the "DZ Bank Magazine" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018
 *
 ***/

/**
 * Magazine
 */
class Magazine extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * number
     *
     * @var int
     */
    protected $number = '';
    /**
     * title
     *
     * @var string
     */
    protected $title = '';

    /**
     * subtitle
     *
     * @var string
     */
    protected $subtitle = '';

    /**
     * externalUrl
     *
     * @var string
     */
    protected $externalUrl = '';

    /**
     * externalTitle
     *
     * @var string
     */
    protected $externalTitle = '';

    /**
     * externalDescription
     *
     * @var string
     */
    protected $externalDescription = '';

    /**
     * article
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Netfed\Dzbankmagazine\Domain\Model\Article>
     * @cascade remove
     */
    protected $article = null;

    /**
     * rubric
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Netfed\Dzbankmagazine\Domain\Model\Rubric>
     */
    protected $rubric = null;

    /**
     * image
     *
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     */
    protected $image = null;

    /**
     * imageHover
     *
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     */
    protected $imageHover;

    /**
     * date
     *
     * @var \DateTime
     */
    protected $date = null;

    /**
     * __construct
     */
    public function __construct()
    {
        //Do not remove the next line: It would break the functionality
        $this->initStorageObjects();
    }

    /**
     * Initializes all ObjectStorage properties
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     *
     * @return void
     */
    protected function initStorageObjects()
    {
        $this->article = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $this->rubric = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }

    /**
     * Returns the number
     *
     * @return string $number
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Sets the number
     *
     * @param string $number
     * @return void
     */
    public function setNumber($number)
    {
        $this->number = $number;
    }

    /**
     * Returns the title
     *
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets the title
     *
     * @param string $title
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Returns the externalTitle
     *
     * @return string $externalTitle
     */
    public function getExternalTitle()
    {
        return $this->externalTitle;
    }

    /**
     * Sets the externalTitle
     *
     * @param string $externalTitle
     * @return void
     */
    public function setExternalTitle($externalTitle)
    {
        $this->externalTitle = $externalTitle;
    }

    /**
     * Returns the externalDescription
     *
     * @return string $externalDescription
     */
    public function getExternalDescription()
    {
        return $this->externalDescription;
    }

    /**
     * Sets the externalDescription
     *
     * @param string $externalDescription
     * @return void
     */
    public function setExternalDescription($externalDescription)
    {
        $this->externalDescription = $externalDescription;
    }

    /**
     * Returns the externalUrl
     *
     * @return string $externalUrl
     */
    public function getExternalUrl()
    {
        return $this->externalUrl;
    }

    /**
     * Sets the externalUrl
     *
     * @param string $externalUrl
     * @return void
     */
    public function setExternalUrl($externalUrl)
    {
        $this->externalUrl = $externalUrl;
    }

    /**
     * Adds a Article
     *
     * @param \Netfed\Dzbankmagazine\Domain\Model\Article $article
     * @return void
     */
    public function addArticle(\Netfed\Dzbankmagazine\Domain\Model\Article $article)
    {
        $this->article->attach($article);
    }

    /**
     * Removes a Article
     *
     * @param \Netfed\Dzbankmagazine\Domain\Model\Article $articleToRemove The Article to be removed
     * @return void
     */
    public function removeArticle(\Netfed\Dzbankmagazine\Domain\Model\Article $articleToRemove)
    {
        $this->article->detach($articleToRemove);
    }

    /**
     * Returns the article
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Netfed\Dzbankmagazine\Domain\Model\Article> $article
     */
    public function getArticle()
    {
        return $this->article;
    }

    /**
     * Sets the article
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Netfed\Dzbankmagazine\Domain\Model\Article> $article
     * @return void
     */
    public function setArticle(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $article)
    {
        $this->article = $article;
    }

    /**
     * Returns the article
     *
     * @return \Netfed\Dzbankmagazine\Domain\Model\Article $article
     */
    public function getTopArticle()
    {
        foreach ($this->article as $article) {
            if ($article->isTop()) {
                return $article;
            }
        }
        return null;
    }

    /**
     * Returns the article
     *
     * @return \Netfed\Dzbankmagazine\Domain\Model\Article $article
     */
    public function getEditorialArticle()
    {
        foreach ($this->article as $article) {
            if ($article->isEditorial()) {
                return $article;
            }
        }
        return null;
    }

    /**
     * Returns the subtitle
     *
     * @return string $subtitle
     */
    public function getSubtitle()
    {
        return $this->subtitle;
    }

    /**
     * Sets the subtitle
     *
     * @param string $subtitle
     * @return void
     */
    public function setSubtitle($subtitle)
    {
        $this->subtitle = $subtitle;
    }

    /**
     * Returns the rubric
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Netfed\Dzbankmagazine\Domain\Model\Rubric> $rubric
     */
    public function getRubric()
    {
        return $this->rubric;
    }

    /**
     * Sets the rubric
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Netfed\Dzbankmagazine\Domain\Model\Rubric> $rubric
     * @return void
     */
    public function setRubric(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $rubric)
    {
        $this->rubric = $rubric;
    }

    /**
     * Returns the image
     *
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $image
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Sets the image
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $image
     * @return void
     */
    public function setImage(\TYPO3\CMS\Extbase\Domain\Model\FileReference $image)
    {
        $this->image = $image;
    }

    /**
     * Returns the imageHover
     *
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $imageHover
     */
    public function getImageHover()
    {
        return $this->imageHover;
    }

    /**
     * Sets the imageHover
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $imageHover
     * @return void
     */
    public function setImageHover(\TYPO3\CMS\Extbase\Domain\Model\FileReference $imageHover)
    {
        $this->imageHover = $imageHover;
    }

    /**
     * Returns the date
     *
     * @return \DateTime $date
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Sets the date
     *
     * @param \DateTime $date
     * @return void
     */
    public function setDate(\DateTime $date)
    {
        $this->date = $date;
    }
}
