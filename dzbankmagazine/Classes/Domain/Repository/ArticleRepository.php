<?php
namespace Netfed\Dzbankmagazine\Domain\Repository;

/***
 *
 * This file is part of the "DZ Bank Magazine" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018
 *
 ***/
use Netfed\Dzbankmagazine\Domain\Model\Magazine;
use Netfed\Dzbankmagazine\Domain\Model\Rubric;
use Netfed\Dzbankmagazine\Domain\Model\Theme;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * The repository for Articles
 */
class ArticleRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{
    /**
     * @var array
     */
    protected $defaultOrderings = [
        'sorting' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING
    ];

    /**
     * find Articles By uids
     *
     * @param string|array $uids
     * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
     */
    public function findByUids($uids){
        $uids = is_array($uids) ? $uids : GeneralUtility::intExplode(',', $uids);
        $query = $this->createQuery();
        $query->setQuerySettings($query->getQuerySettings()->setRespectStoragePage(false));
        $query->matching($query->in('uid', $uids));
        return $query->execute();
    }

    /**
     * find Articles By Magazine with exclude list
     *
     * @param Magazine $magazine
     * @param array $exclude
     * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
     */
    public function findByMagazine(Magazine $magazine, array $exclude=[]){
        $query = $this->createQuery();
        $query->matching(
            $query->logicalAnd(
                $query->contains('magazine', $magazine),
                $query->logicalNot(
                    $query->in('uid', $exclude)
                )
            )
        );
        return $query->execute();
    }

    /**
     * find Articles By Rubric with exclude list
     *
     * @param Rubric $rubric
     * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
     */
    public function findByRubric(Rubric $rubric){
        $query = $this->createQuery();
        $query->matching($query->contains('rubric', $rubric));
        return $query->execute();
    }

    /**
     * find Articles By Theme with exclude list
     *
     * @param Theme $theme
     * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
     */
    public function findByTheme(Theme $theme){
        $query = $this->createQuery();
        $query->matching($query->contains('theme', $theme));
        return $query->execute();
    }

    /**
     * search Articles
     *
     * @param string $word
     * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
     */
    public function search($word){
        $query = $this->createQuery();
        $query->matching(
            $query->logicalOr(
                $query->like('title', "%$word%"),
                $query->like('subtitle', "%$word%"),
                $query->like('content.bodytext', "%$word%")
            )
        );
        return $query->execute();
    }
}
