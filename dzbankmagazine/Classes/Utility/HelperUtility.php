<?php
namespace Netfed\Dzbankmagazine\Utility;

/***
 *
 * This file is part of the "DZ Bank Users" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018
 *
 ***/

/**
 * The Helper utility
 */
class HelperUtility
{
    /**
     * Recursively extract the key
     *
     * @param \Traversable $iterator
     * @param string $key
     * @return string
     * @throws \Exception
     */
    public static function recursivelyExtractKey($iterator, $key)
    {
        $content = [];

        foreach ($iterator as $v) {
            $result = \TYPO3\CMS\Extbase\Reflection\ObjectAccess::getPropertyPath($v, $key);
            if (null !== $result) {
                $content[] = $result;
            } elseif (true === is_array($v) || true === $v instanceof \Traversable) {
                $content[] = static::recursivelyExtractKey($v, $key);
            }
        }

        $content = static::flattenArray($content);

        return $content;
    }

    /**
     * Flatten the result structure, to iterate it cleanly in fluid
     *
     * @param array $content
     * @param array $flattened
     * @return array
     */
    public static function flattenArray(array $content, $flattened = null)
    {
        foreach ($content as $sub) {
            if (is_array($sub)) {
                $flattened = static::flattenArray($sub, $flattened);
            } else {
                $flattened[] = $sub;
            }
        }

        return $flattened;
    }
}