<?php

namespace Netfed\Dzbankmagazine\Hooks;


use TYPO3\CMS\Core\Utility\GeneralUtility;

class FeLogin
{
    public $forceSetCookie = TRUE;
    public $loginType = '';

    public function hook_beforeRedirect(array $params = array (), \TYPO3\CMS\Felogin\Controller\FrontendLoginController $frontendLoginController = NULL)
    {
        if (TYPO3_MODE === 'FE') {
            $this->loginType = $params['loginType'];
            if ($this->loginType === 'login') {
                /** @var \TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController $tsfe */
                $tsfe = $GLOBALS['TSFE'];
                $extConf = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['dzbankmagazine']);
                if ($tsfe->loginUser) {
                    $articleParams['tx_dzbankmagazine_magazine'] = GeneralUtility::_GP('tx_dzbankmagazine_magazine');
                    $params['redirectUrl'] = $params['redirectUrl'] . '&' . http_build_query($articleParams);
                    $this->redirectUrl = $params['redirectUrl'];
                    \TYPO3\CMS\Core\Utility\HttpUtility::redirect($this->redirectUrl);
                }
            }
        }
    }
}