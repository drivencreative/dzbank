<?php
namespace Netfed\Dzbankmagazine\Controller;

/***
 *
 * This file is part of the "DZ Bank Magazine" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018
 *
 ***/

/**
 * MagazineController
 */
class MagazineController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * magazineRepository
     *
     * @var \Netfed\Dzbankmagazine\Domain\Repository\MagazineRepository
     * @inject
     */
    protected $magazineRepository = null;

    /**
     * articleRepository
     *
     * @var \Netfed\Dzbankmagazine\Domain\Repository\ArticleRepository
     * @inject
     */
    protected $articleRepository = null;

    /**
     * action list
     *
     * @return void
     */
    public function listAction()
    {
        $magazines = $this->magazineRepository->findAll();
        $this->view->assign('magazines', $magazines);
    }

    /**
     * action show
     *
     * @param \Netfed\Dzbankmagazine\Domain\Model\Magazine $magazine
     * @return void
     */
    public function showAction(\Netfed\Dzbankmagazine\Domain\Model\Magazine $magazine)
    {
        $this->view->assign('magazine', $magazine);
    }

    /**
     * action index
     *
     * @param \Netfed\Dzbankmagazine\Domain\Model\Magazine $magazine
     * @return void
     */
    public function indexAction(\Netfed\Dzbankmagazine\Domain\Model\Magazine $magazine = null)
    {
        if(!$magazine){
            $magazine = $this->magazineRepository->findFirst();
        }

        $this->view->assign('magazine', $magazine);
        $this->view->assign('topArticle', $magazine->getTopArticle());
        $this->view->assign('editorialArticle', $magazine->getEditorialArticle());
        $this->view->assign('gridArticles', $this->articleRepository->findByMagazine($magazine,[$magazine->getTopArticle(), $magazine->getEditorialArticle()])->toArray());
    }

    /**
     * action header
     *
     * @param \Netfed\Dzbankmagazine\Domain\Model\Magazine $magazine
     * @return void
     */
    public function headerAction(\Netfed\Dzbankmagazine\Domain\Model\Magazine $magazine = null)
    {
        if(!$magazine){
            $magazine = $this->magazineRepository->findFirst();
        }

        $this->view->assign('topArticle', $magazine->getTopArticle());
    }
}
