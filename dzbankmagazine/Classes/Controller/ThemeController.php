<?php
namespace Netfed\Dzbankmagazine\Controller;

/***
 *
 * This file is part of the "DZ Bank Magazine" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018
 *
 ***/

/**
 * ThemeController
 */
class ThemeController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * themeRepository
     *
     * @var \Netfed\Dzbankmagazine\Domain\Repository\ThemeRepository
     * @inject
     */
    protected $themeRepository = null;

    /**
     * articleRepository
     *
     * @var \Netfed\Dzbankmagazine\Domain\Repository\ArticleRepository
     * @inject
     */
    protected $articleRepository = null;

    /**
     * action list
     *
     * @return void
     */
    public function listAction()
    {
        $themes = $this->themeRepository->findAll();
        $this->view->assign('themes', $themes);
    }

    /**
     * action show
     *
     * @param \Netfed\Dzbankmagazine\Domain\Model\Theme $theme
     * @return void
     */
    public function showAction(\Netfed\Dzbankmagazine\Domain\Model\Theme $theme)
    {
        $articles = $this->articleRepository->findByTheme($theme);
        $this->view->assign('articles', $articles);
        $this->view->assign('theme', $theme);
    }
}
