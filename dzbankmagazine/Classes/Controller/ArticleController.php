<?php
namespace Netfed\Dzbankmagazine\Controller;

/***
 *
 * This file is part of the "DZ Bank Magazine" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018
 *
 ***/

/**
 * ArticleController
 */
class ArticleController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * articleRepository
     *
     * @var \Netfed\Dzbankmagazine\Domain\Repository\ArticleRepository
     * @inject
     */
    protected $articleRepository = null;

    /**
     * action list
     *
     * @return void
     */
    public function listAction()
    {
        $articles = $this->articleRepository->findAll();
        $this->view->assign('articles', $articles);
    }

    /**
     * action show
     *
     * @param \Netfed\Dzbankmagazine\Domain\Model\Article $article
     * @return void
     */
    public function showAction(\Netfed\Dzbankmagazine\Domain\Model\Article $article)
    {
        if ($GLOBALS['TSFE']->fe_user->user['uid']) {
            $article->addReaders($this->objectManager->get(\Netfed\Dzbankuser\Domain\Repository\UserRepository::class)->findByUid($GLOBALS['TSFE']->fe_user->user['uid']));

            $this->articleRepository->update($article);
        } else{
            $this->redirect(null, null, null, $this->request->getArguments(), $this->settings['login']);
        }
        $this->view->assign('article', $article);
    }
}
