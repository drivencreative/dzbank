<?php
namespace Netfed\Dzbankmagazine\Controller;

/***
 *
 * This file is part of the dzbankmagazine Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017
 *
 ***/
use TYPO3\CMS\Core\Crypto\Random;
use TYPO3\CMS\Core\Database\Connection;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\Restriction\FrontendRestrictionContainer;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

/**
 * UserController
 */
class FrontendLoginController extends \TYPO3\CMS\Felogin\Controller\FrontendLoginController
{

    /**
     * The main method of the plugin
     *
     * @param string $content The PlugIn content
     * @param array $conf The PlugIn configuration
     * @return string The content that is displayed on the website
     * @throws \RuntimeException when no storage PID was configured.
     */
    public function main($content, $conf)
    {
        // Loading TypoScript array into object variable:
        $this->conf = $conf;
        // Loading default pivars
        $this->pi_setPiVarDefaults();
        // Loading language-labels
        $this->pi_loadLL('EXT:felogin/Resources/Private/Language/locallang.xlf');
        // Init FlexForm configuration for plugin:
        $this->pi_initPIflexForm();
        $this->mergeflexFormValuesIntoConf();
        // Get storage PIDs:
        if ($this->conf['storagePid']) {
            if ((int)$this->conf['recursive']) {
                $this->spid = $this->pi_getPidList($this->conf['storagePid'], (int)$this->conf['recursive']);
            } else {
                $this->spid = $this->conf['storagePid'];
            }
        } else {
            throw new \RuntimeException('No storage folder (option storagePid) for frontend users given.', 1450904202);
        }
        // GPvars:
        $this->logintype = GeneralUtility::_GP('logintype');
        $this->referer = $this->validateRedirectUrl(GeneralUtility::_GP('referer'));
        $this->noRedirect = $this->piVars['noredirect'] || $this->conf['redirectDisable'];
        // If config.typolinkLinkAccessRestrictedPages is set, the var is return_url
        $returnUrl = GeneralUtility::_GP('return_url');
        if ($returnUrl) {
            $this->redirectUrl = $returnUrl;
        } else {
            $this->redirectUrl = GeneralUtility::_GP('redirect_url');
        }
        $this->redirectUrl = $this->validateRedirectUrl($this->redirectUrl);
        // Get Template
        $templateFile = $this->conf['templateFile'] ?: 'EXT:felogin/Resources/Private/Templates/FrontendLogin.html';
        $template = GeneralUtility::getFileAbsFileName($templateFile);
        if ($template !== '' && file_exists($template)) {
            $this->template = file_get_contents($template);
        }
        // Is user logged in?
        $this->userIsLoggedIn = $this->frontendController->loginUser;
        $this->noRedirect = !$this->userIsLoggedIn;
        // Redirect
        if ($this->conf['redirectMode'] && !$this->conf['redirectDisable'] && !$this->noRedirect && !$this->redirectUrl) {
            $redirectUrl = $this->processRedirect();
            if (!empty($redirectUrl)) {
                $this->redirectUrl = $this->conf['redirectFirstMethod'] ? array_shift($redirectUrl) : array_pop($redirectUrl);
            } else {
                $this->redirectUrl = '';
            }
        }
        // What to display
        $content = '';
        if ($this->piVars['forgot'] && $this->conf['showForgotPasswordLink']) {
            $content .= $this->showForgot();
        } elseif ($this->piVars['forgothash']) {
            $content .= $this->changePassword();
        } else {
            if ($this->userIsLoggedIn && !$this->logintype) {
                $content .= $this->showLogout();
            } else {
                $content .= $this->showLogin();
            }
        }
        // Process the redirect
        if (($this->logintype === 'login' || $this->logintype === 'logout') && $this->redirectUrl && !$this->noRedirect) {

            if (!$this->frontendController->fe_user->isCookieSet() && $this->userIsLoggedIn) {
                $content .= $this->cObj->stdWrap($this->pi_getLL('cookie_warning'), $this->conf['cookieWarning_stdWrap.']);
            } else {
                // Add hook for extra processing before redirect

                if (
                    isset($GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['felogin']['beforeRedirect']) &&
                    is_array($GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['felogin']['beforeRedirect'])
                ) {
                    $_params = [
                        'loginType' => $this->logintype,
                        'redirectUrl' => &$this->redirectUrl
                    ];
                    foreach ($GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['felogin']['beforeRedirect'] as $_funcRef) {
                        if ($_funcRef) {
                            GeneralUtility::callUserFunction($_funcRef, $_params, $this);
                        }
                    }
                }
                \TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($this->redirectUrl,'redirectUrl');die;
                \TYPO3\CMS\Core\Utility\HttpUtility::redirect($this->redirectUrl);
            }
        }

        // Adds hook for processing of extra item markers / special
        if (
            isset($GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['felogin']['postProcContent'])
            && is_array($GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['felogin']['postProcContent'])
        ) {
            $_params = [
                'content' => $content
            ];
            foreach ($GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['felogin']['postProcContent'] as $_funcRef) {
                $content = GeneralUtility::callUserFunction($_funcRef, $_params, $this);
            }
        }
        return $this->conf['wrapContentInBaseClass'] ? $this->pi_wrapInBaseClass($content) : $content;
    }

}
