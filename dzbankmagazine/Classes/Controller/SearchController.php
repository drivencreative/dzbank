<?php
namespace Netfed\Dzbankmagazine\Controller;

/***
 *
 * This file is part of the "DZ Bank Magazine" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018
 *
 ***/

/**
 * SearchController
 */
class SearchController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * articleRepository
     *
     * @var \Netfed\Dzbankmagazine\Domain\Repository\ArticleRepository
     * @inject
     */
    protected $articleRepository = null;

    /**
     * action index
     *
     * @param string $word
     * @return void
     */
    public function indexAction($word = '')
    {
        $articles = $this->articleRepository->search($word);
        $this->view->assign('articles', $articles);
        $this->view->assign('word', $word);
    }

}
