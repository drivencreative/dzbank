<?php
namespace Netfed\Dzbankmagazine\Controller;

/***
 *
 * This file is part of the "DZ Bank Magazine" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018
 *
 ***/

/**
 * RubricController
 */
class RubricController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * rubricRepository
     *
     * @var \Netfed\Dzbankmagazine\Domain\Repository\RubricRepository
     * @inject
     */
    protected $rubricRepository = null;

    /**
     * articleRepository
     *
     * @var \Netfed\Dzbankmagazine\Domain\Repository\ArticleRepository
     * @inject
     */
    protected $articleRepository = null;

    /**
     * action list
     *
     * @return void
     */
    public function listAction()
    {
        $rubrics = $this->rubricRepository->findAll();
        $this->view->assign('rubrics', $rubrics);
    }

    /**
     * action show
     *
     * @param \Netfed\Dzbankmagazine\Domain\Model\Rubric $rubric
     * @return void
     */
    public function showAction(\Netfed\Dzbankmagazine\Domain\Model\Rubric $rubric)
    {
        $articles = $this->articleRepository->findByRubric($rubric);
        $this->view->assign('articles', $articles);
        $this->view->assign('rubric', $rubric);
    }
}
