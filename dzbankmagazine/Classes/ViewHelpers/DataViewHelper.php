<?php

namespace Netfed\Dzbankmagazine\ViewHelpers;

/**
 * Class DataViewHelper
 *
 * @package Netfed\Dzbankmagazine\ViewHelpers
 */
class DataViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper
{
    /**
     * Arguments Initialization
     */
    public function initializeArguments()
    {
        $this->registerArgument(
            'classString',
            'string',
            'The class namespace to resolve object',
            false,
            'Netfed\Dzbankmagazine\Domain\Repository\RubricRepository'
        );
        $this->registerArgument(
            'methodName',
            'string',
            'The method namespace to call function',
            false,
            'findAll'
        );
        $this->registerArgument(
            'parameters',
            'array',
            'Optional parameters for method',
            false,
            []
        );
    }

    /**
     * Get Data
     *
     * @return string
     */
    public function render()
    {
        $classString = $this->arguments['classString'];
        $methodName = $this->arguments['methodName'];
        if (isset($classString) && isset($methodName)) {
            return $this->objectManager->get($classString)->$methodName(...$this->arguments['parameters']);
        }
    }

}