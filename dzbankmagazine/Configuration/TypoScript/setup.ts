
plugin.tx_dzbankmagazine_magazine {
    view {
        templateRootPaths.0 = EXT:dzbankmagazine/Resources/Private/Templates/
        templateRootPaths.1 = {$plugin.tx_dzbankmagazine_magazine.view.templateRootPath}
        partialRootPaths.0 = EXT:dzbankmagazine/Resources/Private/Partials/
        partialRootPaths.1 = {$plugin.tx_dzbankmagazine_magazine.view.partialRootPath}
        layoutRootPaths.0 = EXT:dzbankmagazine/Resources/Private/Layouts/
        layoutRootPaths.1 = {$plugin.tx_dzbankmagazine_magazine.view.layoutRootPath}
    }
    persistence {
        storagePid = {$plugin.tx_dzbankmagazine_magazine.persistence.storagePid}
        recursive = 1
    }
    features {
        #skipDefaultArguments = 1
        # if set to 1, the enable fields are ignored in BE context
        ignoreAllEnableFieldsInBe = 0
        # Should be on by default, but can be disabled if all action in the plugin are uncached
        requireCHashArgumentForActionArguments = 1
    }
    mvc {
        #callDefaultActionIfActionCantBeResolved = 1
    }
    settings {
        show {
            article = {$plugin.tx_dzbankmagazine_magazine.settings.show.article}
            rubric = {$plugin.tx_dzbankmagazine_magazine.settings.show.rubric}
            theme = {$plugin.tx_dzbankmagazine_magazine.settings.show.theme}
        }
        login = {$plugin.tx_dzbankmagazine_magazine.settings.login}
    }
}

plugin.tx_felogin_pi1 {
    preserveGETvars = all
    showPermaLogin = 1
    redirectFirstMethod = 1
    showLogoutFormAfterLogin = 0
    redirectMode = getpost,referer,login,logout,loginError
}

#config.typolinkLinkAccessRestrictedPages = 1
#config.typolinkLinkAccessRestrictedPages_addParams = &referer=###RETURN_URL###

# these classes are only used in auto-generated templates
plugin.tx_dzbankmagazine._CSS_DEFAULT_STYLE (
    textarea.f3-form-error {
        background-color:#FF9F9F;
        border: 1px #FF0000 solid;
    }

    input.f3-form-error {
        background-color:#FF9F9F;
        border: 1px #FF0000 solid;
    }

    .tx-dzbankmagazine table {
        border-collapse:separate;
        border-spacing:10px;
    }

    .tx-dzbankmagazine table th {
        font-weight:bold;
    }

    .tx-dzbankmagazine table td {
        vertical-align:top;
    }

    .typo3-messages .message-error {
        color:red;
    }

    .typo3-messages .message-ok {
        color:green;
    }
)

lib.show.article = TEXT
lib.show.article.value = {$plugin.tx_dzbankmagazine_magazine.settings.show.article}
lib.show.rubric = TEXT
lib.show.rubric.value = {$plugin.tx_dzbankmagazine_magazine.settings.show.rubric}
lib.show.theme = TEXT
lib.show.theme.value = {$plugin.tx_dzbankmagazine_magazine.settings.show.theme}