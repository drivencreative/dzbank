
plugin.tx_dzbankmagazine_magazine {
    view {
        # cat=plugin.tx_dzbankmagazine_magazine/file; type=string; label=Path to template root (FE)
        templateRootPath = EXT:dzbankmagazine/Resources/Private/Templates/
        # cat=plugin.tx_dzbankmagazine_magazine/file; type=string; label=Path to template partials (FE)
        partialRootPath = EXT:dzbankmagazine/Resources/Private/Partials/
        # cat=plugin.tx_dzbankmagazine_magazine/file; type=string; label=Path to template layouts (FE)
        layoutRootPath = EXT:dzbankmagazine/Resources/Private/Layouts/
    }
    persistence {
        # cat=plugin.tx_dzbankmagazine_magazine//a; type=string; label=Default storage PID
        storagePid = 3
    }
    settings {
        show {
            article = 18
            rubric = 19
            theme = 20
        }
        login = 4
    }
}