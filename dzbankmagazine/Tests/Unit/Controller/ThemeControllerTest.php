<?php
namespace Netfed\Dzbankmagazine\Tests\Unit\Controller;

/**
 * Test case.
 */
class ThemeControllerTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \Netfed\Dzbankmagazine\Controller\ThemeController
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = $this->getMockBuilder(\Netfed\Dzbankmagazine\Controller\ThemeController::class)
            ->setMethods(['redirect', 'forward', 'addFlashMessage'])
            ->disableOriginalConstructor()
            ->getMock();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function listActionFetchesAllThemesFromRepositoryAndAssignsThemToView()
    {

        $allThemes = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->disableOriginalConstructor()
            ->getMock();

        $themeRepository = $this->getMockBuilder(\Netfed\Dzbankmagazine\Domain\Repository\ThemeRepository::class)
            ->setMethods(['findAll'])
            ->disableOriginalConstructor()
            ->getMock();
        $themeRepository->expects(self::once())->method('findAll')->will(self::returnValue($allThemes));
        $this->inject($this->subject, 'themeRepository', $themeRepository);

        $view = $this->getMockBuilder(\TYPO3\CMS\Extbase\Mvc\View\ViewInterface::class)->getMock();
        $view->expects(self::once())->method('assign')->with('themes', $allThemes);
        $this->inject($this->subject, 'view', $view);

        $this->subject->listAction();
    }

    /**
     * @test
     */
    public function showActionAssignsTheGivenThemeToView()
    {
        $theme = new \Netfed\Dzbankmagazine\Domain\Model\Theme();

        $view = $this->getMockBuilder(\TYPO3\CMS\Extbase\Mvc\View\ViewInterface::class)->getMock();
        $this->inject($this->subject, 'view', $view);
        $view->expects(self::once())->method('assign')->with('theme', $theme);

        $this->subject->showAction($theme);
    }
}
