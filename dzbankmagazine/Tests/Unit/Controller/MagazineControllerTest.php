<?php
namespace Netfed\Dzbankmagazine\Tests\Unit\Controller;

/**
 * Test case.
 */
class MagazineControllerTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \Netfed\Dzbankmagazine\Controller\MagazineController
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = $this->getMockBuilder(\Netfed\Dzbankmagazine\Controller\MagazineController::class)
            ->setMethods(['redirect', 'forward', 'addFlashMessage'])
            ->disableOriginalConstructor()
            ->getMock();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function listActionFetchesAllMagazinesFromRepositoryAndAssignsThemToView()
    {

        $allMagazines = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->disableOriginalConstructor()
            ->getMock();

        $magazineRepository = $this->getMockBuilder(\Netfed\Dzbankmagazine\Domain\Repository\MagazineRepository::class)
            ->setMethods(['findAll'])
            ->disableOriginalConstructor()
            ->getMock();
        $magazineRepository->expects(self::once())->method('findAll')->will(self::returnValue($allMagazines));
        $this->inject($this->subject, 'magazineRepository', $magazineRepository);

        $view = $this->getMockBuilder(\TYPO3\CMS\Extbase\Mvc\View\ViewInterface::class)->getMock();
        $view->expects(self::once())->method('assign')->with('magazines', $allMagazines);
        $this->inject($this->subject, 'view', $view);

        $this->subject->listAction();
    }

    /**
     * @test
     */
    public function showActionAssignsTheGivenMagazineToView()
    {
        $magazine = new \Netfed\Dzbankmagazine\Domain\Model\Magazine();

        $view = $this->getMockBuilder(\TYPO3\CMS\Extbase\Mvc\View\ViewInterface::class)->getMock();
        $this->inject($this->subject, 'view', $view);
        $view->expects(self::once())->method('assign')->with('magazine', $magazine);

        $this->subject->showAction($magazine);
    }
}
