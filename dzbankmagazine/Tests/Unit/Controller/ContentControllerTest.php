<?php
namespace Netfed\Dzbankmagazine\Tests\Unit\Controller;

/**
 * Test case.
 */
class ContentControllerTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \Netfed\Dzbankmagazine\Controller\ContentController
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = $this->getMockBuilder(\Netfed\Dzbankmagazine\Controller\ContentController::class)
            ->setMethods(['redirect', 'forward', 'addFlashMessage'])
            ->disableOriginalConstructor()
            ->getMock();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function listActionFetchesAllContentsFromRepositoryAndAssignsThemToView()
    {

        $allContents = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->disableOriginalConstructor()
            ->getMock();

        $contentRepository = $this->getMockBuilder(\Netfed\Dzbankmagazine\Domain\Repository\ContentRepository::class)
            ->setMethods(['findAll'])
            ->disableOriginalConstructor()
            ->getMock();
        $contentRepository->expects(self::once())->method('findAll')->will(self::returnValue($allContents));
        $this->inject($this->subject, 'contentRepository', $contentRepository);

        $view = $this->getMockBuilder(\TYPO3\CMS\Extbase\Mvc\View\ViewInterface::class)->getMock();
        $view->expects(self::once())->method('assign')->with('contents', $allContents);
        $this->inject($this->subject, 'view', $view);

        $this->subject->listAction();
    }

    /**
     * @test
     */
    public function showActionAssignsTheGivenContentToView()
    {
        $content = new \Netfed\Dzbankmagazine\Domain\Model\Content();

        $view = $this->getMockBuilder(\TYPO3\CMS\Extbase\Mvc\View\ViewInterface::class)->getMock();
        $this->inject($this->subject, 'view', $view);
        $view->expects(self::once())->method('assign')->with('content', $content);

        $this->subject->showAction($content);
    }
}
