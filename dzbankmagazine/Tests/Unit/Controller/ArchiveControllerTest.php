<?php
namespace Netfed\Dzbankmagazine\Tests\Unit\Controller;

/**
 * Test case.
 */
class ArchiveControllerTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \Netfed\Dzbankmagazine\Controller\ArchiveController
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = $this->getMockBuilder(\Netfed\Dzbankmagazine\Controller\ArchiveController::class)
            ->setMethods(['redirect', 'forward', 'addFlashMessage'])
            ->disableOriginalConstructor()
            ->getMock();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function listActionFetchesAllArchivesFromRepositoryAndAssignsThemToView()
    {

        $allArchives = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->disableOriginalConstructor()
            ->getMock();

        $archiveRepository = $this->getMockBuilder(\Netfed\Dzbankmagazine\Domain\Repository\ArchiveRepository::class)
            ->setMethods(['findAll'])
            ->disableOriginalConstructor()
            ->getMock();
        $archiveRepository->expects(self::once())->method('findAll')->will(self::returnValue($allArchives));
        $this->inject($this->subject, 'archiveRepository', $archiveRepository);

        $view = $this->getMockBuilder(\TYPO3\CMS\Extbase\Mvc\View\ViewInterface::class)->getMock();
        $view->expects(self::once())->method('assign')->with('archives', $allArchives);
        $this->inject($this->subject, 'view', $view);

        $this->subject->listAction();
    }

    /**
     * @test
     */
    public function showActionAssignsTheGivenArchiveToView()
    {
        $archive = new \Netfed\Dzbankmagazine\Domain\Model\Archive();

        $view = $this->getMockBuilder(\TYPO3\CMS\Extbase\Mvc\View\ViewInterface::class)->getMock();
        $this->inject($this->subject, 'view', $view);
        $view->expects(self::once())->method('assign')->with('archive', $archive);

        $this->subject->showAction($archive);
    }
}
