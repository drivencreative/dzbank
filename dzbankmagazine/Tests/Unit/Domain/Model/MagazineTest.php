<?php
namespace Netfed\Dzbankmagazine\Tests\Unit\Domain\Model;

/**
 * Test case.
 */
class MagazineTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \Netfed\Dzbankmagazine\Domain\Model\Magazine
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \Netfed\Dzbankmagazine\Domain\Model\Magazine();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getTitleReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getTitle()
        );
    }

    /**
     * @test
     */
    public function setTitleForStringSetsTitle()
    {
        $this->subject->setTitle('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'title',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getSubtitleReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getSubtitle()
        );
    }

    /**
     * @test
     */
    public function setSubtitleForStringSetsSubtitle()
    {
        $this->subject->setSubtitle('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'subtitle',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getUrlReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getUrl()
        );
    }

    /**
     * @test
     */
    public function setUrlForStringSetsUrl()
    {
        $this->subject->setUrl('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'url',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getArticleReturnsInitialValueForArticle()
    {
        $newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        self::assertEquals(
            $newObjectStorage,
            $this->subject->getArticle()
        );
    }

    /**
     * @test
     */
    public function setArticleForObjectStorageContainingArticleSetsArticle()
    {
        $article = new \Netfed\Dzbankmagazine\Domain\Model\Article();
        $objectStorageHoldingExactlyOneArticle = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $objectStorageHoldingExactlyOneArticle->attach($article);
        $this->subject->setArticle($objectStorageHoldingExactlyOneArticle);

        self::assertAttributeEquals(
            $objectStorageHoldingExactlyOneArticle,
            'article',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function addArticleToObjectStorageHoldingArticle()
    {
        $article = new \Netfed\Dzbankmagazine\Domain\Model\Article();
        $articleObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['attach'])
            ->disableOriginalConstructor()
            ->getMock();

        $articleObjectStorageMock->expects(self::once())->method('attach')->with(self::equalTo($article));
        $this->inject($this->subject, 'article', $articleObjectStorageMock);

        $this->subject->addArticle($article);
    }

    /**
     * @test
     */
    public function removeArticleFromObjectStorageHoldingArticle()
    {
        $article = new \Netfed\Dzbankmagazine\Domain\Model\Article();
        $articleObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['detach'])
            ->disableOriginalConstructor()
            ->getMock();

        $articleObjectStorageMock->expects(self::once())->method('detach')->with(self::equalTo($article));
        $this->inject($this->subject, 'article', $articleObjectStorageMock);

        $this->subject->removeArticle($article);
    }
}
