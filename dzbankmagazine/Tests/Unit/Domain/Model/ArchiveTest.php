<?php
namespace Netfed\Dzbankmagazine\Tests\Unit\Domain\Model;

/**
 * Test case.
 */
class ArchiveTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \Netfed\Dzbankmagazine\Domain\Model\Archive
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \Netfed\Dzbankmagazine\Domain\Model\Archive();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getTitleReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getTitle()
        );
    }

    /**
     * @test
     */
    public function setTitleForStringSetsTitle()
    {
        $this->subject->setTitle('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'title',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getSubtitleReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getSubtitle()
        );
    }

    /**
     * @test
     */
    public function setSubtitleForStringSetsSubtitle()
    {
        $this->subject->setSubtitle('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'subtitle',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getDescriptionReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getDescription()
        );
    }

    /**
     * @test
     */
    public function setDescriptionForStringSetsDescription()
    {
        $this->subject->setDescription('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'description',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getDateReturnsInitialValueForDateTime()
    {
        self::assertEquals(
            null,
            $this->subject->getDate()
        );
    }

    /**
     * @test
     */
    public function setDateForDateTimeSetsDate()
    {
        $dateTimeFixture = new \DateTime();
        $this->subject->setDate($dateTimeFixture);

        self::assertAttributeEquals(
            $dateTimeFixture,
            'date',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getMagazineReturnsInitialValueForMagazine()
    {
        $newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        self::assertEquals(
            $newObjectStorage,
            $this->subject->getMagazine()
        );
    }

    /**
     * @test
     */
    public function setMagazineForObjectStorageContainingMagazineSetsMagazine()
    {
        $magazine = new \Netfed\Dzbankmagazine\Domain\Model\Magazine();
        $objectStorageHoldingExactlyOneMagazine = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $objectStorageHoldingExactlyOneMagazine->attach($magazine);
        $this->subject->setMagazine($objectStorageHoldingExactlyOneMagazine);

        self::assertAttributeEquals(
            $objectStorageHoldingExactlyOneMagazine,
            'magazine',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function addMagazineToObjectStorageHoldingMagazine()
    {
        $magazine = new \Netfed\Dzbankmagazine\Domain\Model\Magazine();
        $magazineObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['attach'])
            ->disableOriginalConstructor()
            ->getMock();

        $magazineObjectStorageMock->expects(self::once())->method('attach')->with(self::equalTo($magazine));
        $this->inject($this->subject, 'magazine', $magazineObjectStorageMock);

        $this->subject->addMagazine($magazine);
    }

    /**
     * @test
     */
    public function removeMagazineFromObjectStorageHoldingMagazine()
    {
        $magazine = new \Netfed\Dzbankmagazine\Domain\Model\Magazine();
        $magazineObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['detach'])
            ->disableOriginalConstructor()
            ->getMock();

        $magazineObjectStorageMock->expects(self::once())->method('detach')->with(self::equalTo($magazine));
        $this->inject($this->subject, 'magazine', $magazineObjectStorageMock);

        $this->subject->removeMagazine($magazine);
    }
}
