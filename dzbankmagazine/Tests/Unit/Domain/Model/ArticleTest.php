<?php
namespace Netfed\Dzbankmagazine\Tests\Unit\Domain\Model;

/**
 * Test case.
 */
class ArticleTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \Netfed\Dzbankmagazine\Domain\Model\Article
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \Netfed\Dzbankmagazine\Domain\Model\Article();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getTitleReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getTitle()
        );
    }

    /**
     * @test
     */
    public function setTitleForStringSetsTitle()
    {
        $this->subject->setTitle('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'title',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getSubtitleReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getSubtitle()
        );
    }

    /**
     * @test
     */
    public function setSubtitleForStringSetsSubtitle()
    {
        $this->subject->setSubtitle('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'subtitle',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getRelatedArticlesReturnsInitialValueForArticle()
    {
        $newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        self::assertEquals(
            $newObjectStorage,
            $this->subject->getRelatedArticles()
        );
    }

    /**
     * @test
     */
    public function setRelatedArticlesForObjectStorageContainingArticleSetsRelatedArticles()
    {
        $relatedArticle = new \Netfed\Dzbankmagazine\Domain\Model\Article();
        $objectStorageHoldingExactlyOneRelatedArticles = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $objectStorageHoldingExactlyOneRelatedArticles->attach($relatedArticle);
        $this->subject->setRelatedArticles($objectStorageHoldingExactlyOneRelatedArticles);

        self::assertAttributeEquals(
            $objectStorageHoldingExactlyOneRelatedArticles,
            'relatedArticles',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function addRelatedArticleToObjectStorageHoldingRelatedArticles()
    {
        $relatedArticle = new \Netfed\Dzbankmagazine\Domain\Model\Article();
        $relatedArticlesObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['attach'])
            ->disableOriginalConstructor()
            ->getMock();

        $relatedArticlesObjectStorageMock->expects(self::once())->method('attach')->with(self::equalTo($relatedArticle));
        $this->inject($this->subject, 'relatedArticles', $relatedArticlesObjectStorageMock);

        $this->subject->addRelatedArticle($relatedArticle);
    }

    /**
     * @test
     */
    public function removeRelatedArticleFromObjectStorageHoldingRelatedArticles()
    {
        $relatedArticle = new \Netfed\Dzbankmagazine\Domain\Model\Article();
        $relatedArticlesObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['detach'])
            ->disableOriginalConstructor()
            ->getMock();

        $relatedArticlesObjectStorageMock->expects(self::once())->method('detach')->with(self::equalTo($relatedArticle));
        $this->inject($this->subject, 'relatedArticles', $relatedArticlesObjectStorageMock);

        $this->subject->removeRelatedArticle($relatedArticle);
    }

    /**
     * @test
     */
    public function getRubricReturnsInitialValueForRubric()
    {
        $newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        self::assertEquals(
            $newObjectStorage,
            $this->subject->getRubric()
        );
    }

    /**
     * @test
     */
    public function setRubricForObjectStorageContainingRubricSetsRubric()
    {
        $rubric = new \Netfed\Dzbankmagazine\Domain\Model\Rubric();
        $objectStorageHoldingExactlyOneRubric = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $objectStorageHoldingExactlyOneRubric->attach($rubric);
        $this->subject->setRubric($objectStorageHoldingExactlyOneRubric);

        self::assertAttributeEquals(
            $objectStorageHoldingExactlyOneRubric,
            'rubric',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function addRubricToObjectStorageHoldingRubric()
    {
        $rubric = new \Netfed\Dzbankmagazine\Domain\Model\Rubric();
        $rubricObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['attach'])
            ->disableOriginalConstructor()
            ->getMock();

        $rubricObjectStorageMock->expects(self::once())->method('attach')->with(self::equalTo($rubric));
        $this->inject($this->subject, 'rubric', $rubricObjectStorageMock);

        $this->subject->addRubric($rubric);
    }

    /**
     * @test
     */
    public function removeRubricFromObjectStorageHoldingRubric()
    {
        $rubric = new \Netfed\Dzbankmagazine\Domain\Model\Rubric();
        $rubricObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['detach'])
            ->disableOriginalConstructor()
            ->getMock();

        $rubricObjectStorageMock->expects(self::once())->method('detach')->with(self::equalTo($rubric));
        $this->inject($this->subject, 'rubric', $rubricObjectStorageMock);

        $this->subject->removeRubric($rubric);
    }

    /**
     * @test
     */
    public function getThemeReturnsInitialValueForTheme()
    {
        $newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        self::assertEquals(
            $newObjectStorage,
            $this->subject->getTheme()
        );
    }

    /**
     * @test
     */
    public function setThemeForObjectStorageContainingThemeSetsTheme()
    {
        $theme = new \Netfed\Dzbankmagazine\Domain\Model\Theme();
        $objectStorageHoldingExactlyOneTheme = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $objectStorageHoldingExactlyOneTheme->attach($theme);
        $this->subject->setTheme($objectStorageHoldingExactlyOneTheme);

        self::assertAttributeEquals(
            $objectStorageHoldingExactlyOneTheme,
            'theme',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function addThemeToObjectStorageHoldingTheme()
    {
        $theme = new \Netfed\Dzbankmagazine\Domain\Model\Theme();
        $themeObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['attach'])
            ->disableOriginalConstructor()
            ->getMock();

        $themeObjectStorageMock->expects(self::once())->method('attach')->with(self::equalTo($theme));
        $this->inject($this->subject, 'theme', $themeObjectStorageMock);

        $this->subject->addTheme($theme);
    }

    /**
     * @test
     */
    public function removeThemeFromObjectStorageHoldingTheme()
    {
        $theme = new \Netfed\Dzbankmagazine\Domain\Model\Theme();
        $themeObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['detach'])
            ->disableOriginalConstructor()
            ->getMock();

        $themeObjectStorageMock->expects(self::once())->method('detach')->with(self::equalTo($theme));
        $this->inject($this->subject, 'theme', $themeObjectStorageMock);

        $this->subject->removeTheme($theme);
    }

    /**
     * @test
     */
    public function getContentReturnsInitialValueForContent()
    {
        $newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        self::assertEquals(
            $newObjectStorage,
            $this->subject->getContent()
        );
    }

    /**
     * @test
     */
    public function setContentForObjectStorageContainingContentSetsContent()
    {
        $content = new \Netfed\Dzbankmagazine\Domain\Model\Content();
        $objectStorageHoldingExactlyOneContent = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $objectStorageHoldingExactlyOneContent->attach($content);
        $this->subject->setContent($objectStorageHoldingExactlyOneContent);

        self::assertAttributeEquals(
            $objectStorageHoldingExactlyOneContent,
            'content',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function addContentToObjectStorageHoldingContent()
    {
        $content = new \Netfed\Dzbankmagazine\Domain\Model\Content();
        $contentObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['attach'])
            ->disableOriginalConstructor()
            ->getMock();

        $contentObjectStorageMock->expects(self::once())->method('attach')->with(self::equalTo($content));
        $this->inject($this->subject, 'content', $contentObjectStorageMock);

        $this->subject->addContent($content);
    }

    /**
     * @test
     */
    public function removeContentFromObjectStorageHoldingContent()
    {
        $content = new \Netfed\Dzbankmagazine\Domain\Model\Content();
        $contentObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['detach'])
            ->disableOriginalConstructor()
            ->getMock();

        $contentObjectStorageMock->expects(self::once())->method('detach')->with(self::equalTo($content));
        $this->inject($this->subject, 'content', $contentObjectStorageMock);

        $this->subject->removeContent($content);
    }
}
