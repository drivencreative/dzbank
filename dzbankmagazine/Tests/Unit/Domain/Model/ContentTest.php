<?php
namespace Netfed\Dzbankmagazine\Tests\Unit\Domain\Model;

/**
 * Test case.
 */
class ContentTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \Netfed\Dzbankmagazine\Domain\Model\Content
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \Netfed\Dzbankmagazine\Domain\Model\Content();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getBackgroundReturnsInitialValueForFileReference()
    {
        self::assertEquals(
            null,
            $this->subject->getBackground()
        );
    }

    /**
     * @test
     */
    public function setBackgroundForFileReferenceSetsBackground()
    {
        $fileReferenceFixture = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
        $this->subject->setBackground($fileReferenceFixture);

        self::assertAttributeEquals(
            $fileReferenceFixture,
            'background',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getDescriptionReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getDescription()
        );
    }

    /**
     * @test
     */
    public function setDescriptionForStringSetsDescription()
    {
        $this->subject->setDescription('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'description',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getStoryReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getStory()
        );
    }

    /**
     * @test
     */
    public function setStoryForStringSetsStory()
    {
        $this->subject->setStory('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'story',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getInfoBoxReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getInfoBox()
        );
    }

    /**
     * @test
     */
    public function setInfoBoxForStringSetsInfoBox()
    {
        $this->subject->setInfoBox('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'infoBox',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getInfographicReturnsInitialValueForFileReference()
    {
        $newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        self::assertEquals(
            $newObjectStorage,
            $this->subject->getInfographic()
        );
    }

    /**
     * @test
     */
    public function setInfographicForFileReferenceSetsInfographic()
    {
        $infographic = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
        $objectStorageHoldingExactlyOneInfographic = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $objectStorageHoldingExactlyOneInfographic->attach($infographic);
        $this->subject->setInfographic($objectStorageHoldingExactlyOneInfographic);

        self::assertAttributeEquals(
            $objectStorageHoldingExactlyOneInfographic,
            'infographic',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function addInfographicToObjectStorageHoldingInfographic()
    {
        $infographic = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
        $infographicObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['attach'])
            ->disableOriginalConstructor()
            ->getMock();

        $infographicObjectStorageMock->expects(self::once())->method('attach')->with(self::equalTo($infographic));
        $this->inject($this->subject, 'infographic', $infographicObjectStorageMock);

        $this->subject->addInfographic($infographic);
    }

    /**
     * @test
     */
    public function removeInfographicFromObjectStorageHoldingInfographic()
    {
        $infographic = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
        $infographicObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['detach'])
            ->disableOriginalConstructor()
            ->getMock();

        $infographicObjectStorageMock->expects(self::once())->method('detach')->with(self::equalTo($infographic));
        $this->inject($this->subject, 'infographic', $infographicObjectStorageMock);

        $this->subject->removeInfographic($infographic);
    }

    /**
     * @test
     */
    public function getImageReturnsInitialValueForFileReference()
    {
        self::assertEquals(
            null,
            $this->subject->getImage()
        );
    }

    /**
     * @test
     */
    public function setImageForFileReferenceSetsImage()
    {
        $fileReferenceFixture = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
        $this->subject->setImage($fileReferenceFixture);

        self::assertAttributeEquals(
            $fileReferenceFixture,
            'image',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getTypeReturnsInitialValueForInt()
    {
        self::assertSame(
            0,
            $this->subject->getType()
        );
    }

    /**
     * @test
     */
    public function setTypeForIntSetsType()
    {
        $this->subject->setType(12);

        self::assertAttributeEquals(
            12,
            'type',
            $this->subject
        );
    }
}
