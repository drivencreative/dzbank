<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'Netfed.Dzbankmagazine',
            'Magazine',
            [
                'Magazine' => 'list, show, index, header',
                'Rubric' => 'list, show',
                'Theme' => 'list, show',
                'Archive' => 'list, show',
                'Article' => 'list, show',
                'Content' => 'list, show',
                'Search' => 'index'
            ],
            // non-cacheable actions
            [
                'Magazine' => '',
                'Rubric' => '',
                'Theme' => '',
                'Archive' => '',
                'Article' => '',
                'Content' => '',
                'Search' => 'index'
            ]
        );

    // wizards
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
        'mod {
            wizards.newContentElement.wizardItems.plugins {
                elements {
                    magazine {
                        iconIdentifier = dzbankmagazine-plugin-magazine
                        title = LLL:EXT:dzbankmagazine/Resources/Private/Language/locallang_db.xlf:tx_dzbankmagazine_magazine.name
                        description = LLL:EXT:dzbankmagazine/Resources/Private/Language/locallang_db.xlf:tx_dzbankmagazine_magazine.description
                        tt_content_defValues {
                            CType = list
                            list_type = dzbankmagazine_magazine
                        }
                    }
                }
                show = *
            }
       }'
    );
		$iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
		
			$iconRegistry->registerIcon(
				'dzbankmagazine-plugin-magazine',
				\TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
				['source' => 'EXT:dzbankmagazine/Resources/Public/Icons/user_plugin_magazine.svg']
			);
		
    }
);
$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['felogin']['beforeRedirect']['dzbankmagazine'] = 'Netfed\\Dzbankmagazine\\Hooks\\FeLogin->hook_beforeRedirect';
$GLOBALS['TYPO3_CONF_VARS']['FE']['pageNotFound_handling_loginPageID'] = 4;
//$GLOBALS['TYPO3_CONF_VARS']['FE']['pageNotFound_handling_redirectPageID'] = ''; // ?
//$TYPO3_CONF_VARS['FE']['pageNotFound_handling'] = 'USER_FUNCTION:EXT:dzbankmagazine/Classes/Service/PageNotFound.php:user_pageNotFound->pageNotFound';

$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects']['TYPO3\\CMS\\Felogin\\Controller\\FrontendLoginController'] = ['className' => 'Netfed\\Dzbankmagazine\\Controller\\FrontendLoginController'];