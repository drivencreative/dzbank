<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'Netfed.Dzbankmagazine',
            'Magazine',
            'List'
        );

        $pluginSignature = str_replace('_', '', 'dzbankmagazine') . '_magazine';
        $GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue($pluginSignature, 'FILE:EXT:dzbankmagazine/Configuration/FlexForms/flexform_magazine.xml');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('dzbankmagazine', 'Configuration/TypoScript', 'DZ Bank Magazine');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_dzbankmagazine_domain_model_magazine', 'EXT:dzbankmagazine/Resources/Private/Language/locallang_csh_tx_dzbankmagazine_domain_model_magazine.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_dzbankmagazine_domain_model_magazine');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_dzbankmagazine_domain_model_rubric', 'EXT:dzbankmagazine/Resources/Private/Language/locallang_csh_tx_dzbankmagazine_domain_model_rubric.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_dzbankmagazine_domain_model_rubric');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_dzbankmagazine_domain_model_theme', 'EXT:dzbankmagazine/Resources/Private/Language/locallang_csh_tx_dzbankmagazine_domain_model_theme.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_dzbankmagazine_domain_model_theme');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_dzbankmagazine_domain_model_archive', 'EXT:dzbankmagazine/Resources/Private/Language/locallang_csh_tx_dzbankmagazine_domain_model_archive.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_dzbankmagazine_domain_model_archive');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_dzbankmagazine_domain_model_article', 'EXT:dzbankmagazine/Resources/Private/Language/locallang_csh_tx_dzbankmagazine_domain_model_article.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_dzbankmagazine_domain_model_article');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_dzbankmagazine_domain_model_content', 'EXT:dzbankmagazine/Resources/Private/Language/locallang_csh_tx_dzbankmagazine_domain_model_content.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_dzbankmagazine_domain_model_content');

    }
);
