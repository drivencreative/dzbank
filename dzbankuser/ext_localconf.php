<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'Netfed.Dzbankuser',
            'Subscriber',
            [
                'User' => 'list, show, new, create, edit, update, delete'
            ],
            // non-cacheable actions
            [
                'User' => 'list, show, new, create, edit, update, delete'
            ]
        );

    // wizards
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
        'mod {
            wizards.newContentElement.wizardItems.plugins {
                elements {
                    subscriber {
                        iconIdentifier = dzbankuser-plugin-subscriber
                        title = LLL:EXT:dzbankuser/Resources/Private/Language/locallang_db.xlf:tx_dzbankuser_subscriber.name
                        description = LLL:EXT:dzbankuser/Resources/Private/Language/locallang_db.xlf:tx_dzbankuser_subscriber.description
                        tt_content_defValues {
                            CType = list
                            list_type = dzbankuser_subscriber
                        }
                    }
                }
                show = *
            }
       }'
    );
		$iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
		
			$iconRegistry->registerIcon(
				'dzbankuser-plugin-subscriber',
				\TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
				['source' => 'EXT:dzbankuser/Resources/Public/Icons/user_plugin_subscriber.svg']
			);
		
    }
);
