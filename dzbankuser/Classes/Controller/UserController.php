<?php
namespace Netfed\Dzbankuser\Controller;

/***
 *
 * This file is part of the "DZ Bank Users" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018
 *
 ***/

/**
 * UserController
 */
class UserController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * userRepository
     *
     * @var \Netfed\Dzbankuser\Domain\Repository\UserRepository
     * @inject
     */
    protected $userRepository = null;

    /**
     * userGroupRepository
     *
     * @var \Netfed\Dzbankuser\Domain\Repository\UserGroupRepository
     * @inject
     */
    protected $userGroupRepository = null;

    /**
     * Initialize Action
     */
    public function initializeAction()
    {
        if ($this->configuration = reset($this->arguments)) {
            $this->configuration = $this->configuration->getPropertyMappingConfiguration()
                ->allowAllProperties()
                ->skipUnknownProperties()
                ->setTypeConverterOption(
                    \TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter::class,
                    \TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter::CONFIGURATION_CREATION_ALLOWED,
                    TRUE
                )
                ->setTypeConverterOption(
                    \TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter::class,
                    \TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter::CONFIGURATION_MODIFICATION_ALLOWED,
                    TRUE
                );
        }
    }

    /**
     * action list
     *
     * @return void
     */
    public function listAction()
    {
        $users = $this->userRepository->findByConsultant();
        $this->view->assign('users', $users);
    }

    /**
     * action show
     *
     * @param \Netfed\Dzbankuser\Domain\Model\User $user
     * @return void
     */
    public function showAction(\Netfed\Dzbankuser\Domain\Model\User $user)
    {
        $this->view->assign('user', $user);
    }

    /**
     * action new
     *
     * @return void
     */
    public function newAction()
    {

    }

    /**
     * Initialize Create Action
     */
    public function initializeCreateAction()
    {
        $this->configuration->skipProperties('confirmPassword');
    }

    /**
     * action create
     *
     * @param \Netfed\Dzbankuser\Domain\Model\User $newUser
     * @return void
     */
    public function createAction(\Netfed\Dzbankuser\Domain\Model\User $newUser)
    {
        $groups = array_diff($GLOBALS['TSFE']->fe_user->groupData['uid'], $this->settings['group']);
        $groups[] = $this->settings['group']['user'];

        $newUser->addAllUsergroups($this->userGroupRepository->findByUids($groups));
        $newUser->setPid($this->settings['pid']['protected']);

        $this->view->assign('data', $this->request->getArgument('newUser'));

        $this->sendEmail(
            $newUser->getEmail(),
            $this->settings['sender'],
            $this->settings['subject'],
            $this->cssToInline($this->view->render(), 'EXT:dzbankuser/Resources/Public/Css/email.css')
        );

        $this->addFlashMessage('The object was created.', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::WARNING);
        $this->userRepository->add($newUser);
        $this->redirect('list');
    }

    /**
     * action edit
     *
     * @param \Netfed\Dzbankuser\Domain\Model\User $user
     * @ignorevalidation $user
     * @return void
     */
    public function editAction(\Netfed\Dzbankuser\Domain\Model\User $user)
    {
        $this->view->assign('user', $user);
    }

    /**
     * Initialize Update Action
     */
    public function initializeUpdateAction()
    {
        $request = $this->request->getArguments();
        if (empty($request['user']['password'])) {
            $this->configuration->skipProperties('password', 'confirmPassword');
        }
    }

    /**
     * action update
     *
     * @param \Netfed\Dzbankuser\Domain\Model\User $user
     * @return void
     */
    public function updateAction(\Netfed\Dzbankuser\Domain\Model\User $user)
    {
        $this->addFlashMessage('The object was updated.', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::WARNING);
        $this->userRepository->update($user);
        $this->redirect('list');
    }

    /**
     * action delete
     *
     * @param \Netfed\Dzbankuser\Domain\Model\User $user
     * @return void
     */
    public function deleteAction(\Netfed\Dzbankuser\Domain\Model\User $user)
    {
        $this->addFlashMessage('The object was deleted.', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::WARNING);
        $this->userRepository->remove($user);
        $this->redirect('list');
    }

    /**
     * Create HTML from variables and template path, send email
     *
     * @param string $receiver Email receiver
     * @param string $sender Email sender
     * @param string $subject Email subject
     * @param array $variables objects passed to view
     * @param string $template file name of partial template
     * @param string|array $attachment files to be attached
     * @param string $css file path of css
     */
    protected function sendTemplatedMail($receiver, $sender, $subject, $variables, $template, $attachment = NULL, $css = NULL) {
        $messageBody = $this->getStandaloneView($variables, $template, $css);
        $this->sendEmail($receiver, $sender, $subject, $messageBody, $attachment);
    }

    /**
     * render fluid standalone view
     *
     * @param array $variables objects passed to view
     * @param string $template file name of partial template
     * @param string $css file path of css
     *
     * @return string
     */
    protected function getStandaloneView($variables, $template, $css = NULL) {
        $frameworkConfiguration = $this->configurationManager->getConfiguration(\TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface::CONFIGURATION_TYPE_FRAMEWORK);
        /** @var $standaloneView \TYPO3\CMS\Fluid\View\StandaloneView **/
        $standaloneView = $this->objectManager->get(\TYPO3\CMS\Fluid\View\StandaloneView::class);
        $standaloneView->setControllerContext($this->controllerContext);
        $standaloneView->getRequest()->setControllerExtensionName($this->extensionName);
        $standaloneView->setFormat('html');
        $standaloneView->setLayoutRootPaths($frameworkConfiguration['view']['layoutRootPaths']);
        $standaloneView->setTemplateRootPaths($frameworkConfiguration['view']['templateRootPaths']);
        $standaloneView->setPartialRootPaths($frameworkConfiguration['view']['partialRootPaths']);
        $standaloneView->setTemplate($template);
        $standaloneView->assignMultiple($variables);

        return $this->cssToInline($standaloneView->render(), $css);
    }

    /**
     * Css to Inline
     *
     * @param string $html
     * @param string $css
     *
     * @return string
     **/
    protected function cssToInline($html, $css = '') {
        if ($css = \TYPO3\CMS\Core\Utility\GeneralUtility::getFileAbsFileName($css ?: $this->settings['css'])) {
            $html = $this->objectManager->get(\Netfed\Dzbankuser\Service\CssToInline::class, $html, file_get_contents($css))->emogrify();
        }
        return $html;
    }

    /**
     * Send mail
     *
     * @param string|array $receiver
     * @param string|array $sender
     * @param string $subject
     * @param string $messageBody
     * @param mixed $attachment
     *
     * @return bool
     */
    protected function sendEmail($receiver, $sender, $subject, $messageBody, $attachment = NULL) {
        /** @var $message \TYPO3\CMS\Core\Mail\MailMessage **/
        $message = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Mail\MailMessage::class);
        $message->setTo($receiver)->setFrom($sender)->setSubject($subject)->setBody($messageBody, 'text/html');

        if ($attachment) {
            foreach ((is_array($attachment) ? $attachment : [$attachment]) as $attach) {
                $message->attach(\Swift_Attachment::fromPath($attach));
            }
        }

        $message->send();

        return $message->isSent();
    }

}
