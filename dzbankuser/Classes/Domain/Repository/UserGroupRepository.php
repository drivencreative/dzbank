<?php
namespace Netfed\Dzbankuser\Domain\Repository;

/***
 *
 * This file is part of the "DZ Bank Users" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018
 *
 ***/

/**
 * The repository for UserGroups
 */
class UserGroupRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{
    /**
     * find Users Groups by Uids
     *
     * @param array $uids
     * @return \TYPO3\CMS\Extbase\Persistence\QueryResultInterface
     */
    public function findByUids(array $uids = [])
    {
        $query = $this->createQuery();

        $query->matching($query->in('uid', $uids));

        return $query->execute();
    }
}
