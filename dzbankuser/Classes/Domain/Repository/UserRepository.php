<?php
namespace Netfed\Dzbankuser\Domain\Repository;

/***
 *
 * This file is part of the "DZ Bank Users" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018
 *
 ***/
use Netfed\Dzbankuser\Utility\HelperUtility;

/**
 * The repository for Users
 */
class UserRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{
    /**
     * find Users by Consultant
     *
     * @param array $groups
     * @return \TYPO3\CMS\Extbase\Persistence\QueryResultInterface
     */
    public function findByConsultant(array $groups = [])
    {
        if (!$groups) {
            $groups = $GLOBALS['TSFE']->fe_user->groupData['uid'];
        }

        $settings = HelperUtility::settings();

        $query = $this->createQuery();

        $userGroups = [];
        foreach (array_diff($groups, $settings['group']) as $group) {
            $userGroups[] = $query->contains('usergroup', $group);
        }

        $constraint = [
            $query->contains('usergroup', $settings['group']['user']),
            $query->logicalNot($query->contains('usergroup', $settings['group']['consultant']))
        ];

        if ($userGroups) {
            $constraint[] = $query->logicalOr($userGroups);
        }

        $query->matching($query->logicalAnd($constraint));

        return $query->execute();
    }
}
