<?php
namespace Netfed\Dzbankuser\Domain\Model;

/***
 *
 * This file is part of the "DZ Bank Users" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018
 *
 ***/

/**
 * User
 */
class User extends \TYPO3\CMS\Extbase\Domain\Model\FrontendUser
{

    /**
     * Constructs a new Front-End User
     *
     * @param string $username
     * @param string $password
     * @api
     */
    public function __construct($username = '', $password = '')
    {
        $this->setUsername($username);
        $this->setPassword($password);
        $this->setUsergroup(new \TYPO3\CMS\Extbase\Persistence\ObjectStorage());
    }

    /**
     * Adds all usergroups to the frontend user
     *
     * @param \TYPO3\CMS\Extbase\Persistence\Generic\QueryResult $usergroups
     * @api
     */
    public function addAllUsergroups($usergroups)
    {
        foreach ($usergroups as $usergroup) {
            $this->usergroup->attach($usergroup);
        }
    }

    /**
     * Sets the password value
     *
     * @param string $password
     * @api
     */
    public function setPassword($password)
    {
        $this->password = $this->hashPassword($password);
    }

    /**
     * Hash a password
     *
     * @param string $password
     * @return string|null
     */
    protected function hashPassword($password) {
        if (\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('saltedpasswords')) {
            if (\TYPO3\CMS\Saltedpasswords\Utility\SaltedPasswordsUtility::isUsageEnabled('FE')) {
                $objInstanceSaltedPw = \TYPO3\CMS\Saltedpasswords\Salt\SaltFactory::getSaltingInstance();
                return $objInstanceSaltedPw->getHashedPassword($password);
            }
        }

        return $password;
    }
}
