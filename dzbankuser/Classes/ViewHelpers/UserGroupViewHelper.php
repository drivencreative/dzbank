<?php
namespace Netfed\Dzbankuser\ViewHelpers;

/**
 * Class UserGroupViewHelper
 *
 * @package Netfed\Dzbankuser\ViewHelpers
 */
class UserGroupViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper
{

    /**
     * @return mixed
     */
    public function render()
    {
        $repository = $this->objectManager->get(\Netfed\Dzbankuser\Domain\Repository\UserGroupRepository::class);
        $repository->setDefaultQuerySettings($repository->createQuery()->getQuerySettings()->setRespectStoragePage(false));

        return $repository->findByUid(
            current(array_diff($GLOBALS['TSFE']->fe_user->groupData['uid'], \Netfed\Dzbankuser\Utility\HelperUtility::settings()['group']))
        );
    }

}
