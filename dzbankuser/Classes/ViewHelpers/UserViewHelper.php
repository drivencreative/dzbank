<?php
namespace Netfed\Dzbankuser\ViewHelpers;

/**
 * Class UserViewHelper
 *
 * @package Netfed\Dzbankuser\ViewHelpers
 */
class UserViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper
{

    /**
     * @return mixed
     */
    public function render()
    {
        $repository = $this->objectManager->get(\Netfed\Dzbankuser\Domain\Repository\UserRepository::class);
        $repository->setDefaultQuerySettings($repository->createQuery()->getQuerySettings()->setRespectStoragePage(false));

        return $repository->findByUid($GLOBALS['TSFE']->fe_user->user['uid']);
    }

}
