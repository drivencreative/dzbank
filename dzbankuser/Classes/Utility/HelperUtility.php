<?php
namespace Netfed\Dzbankuser\Utility;

/***
 *
 * This file is part of the "DZ Bank Users" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018
 *
 ***/

/**
 * The Helper utility
 */
class HelperUtility
{
    public static function objectManager($class) {
        return \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\Object\ObjectManager::class)->get($class);
    }

    public static function settings($extension = 'Dzbankuser', $plugin = 'Subscriber')
    {
        $configurationManager = self::objectManager(\TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface::class);
        return $configurationManager->getConfiguration(\TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface::CONFIGURATION_TYPE_SETTINGS, $extension, $plugin);
    }

}