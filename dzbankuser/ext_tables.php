<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'Netfed.Dzbankuser',
            'Subscriber',
            'Subscriber manager'
        );

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('dzbankuser', 'Configuration/TypoScript', 'DZ Bank Users');

    }
);
