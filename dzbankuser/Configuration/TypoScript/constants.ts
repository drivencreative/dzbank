
plugin.tx_dzbankuser_subscriber {
    view {
        # cat=plugin.tx_dzbankuser_subscriber/file; type=string; label=Path to template root (FE)
        templateRootPath = EXT:dzbankuser/Resources/Private/Templates/
        # cat=plugin.tx_dzbankuser_subscriber/file; type=string; label=Path to template partials (FE)
        partialRootPath = EXT:dzbankuser/Resources/Private/Partials/
        # cat=plugin.tx_dzbankuser_subscriber/file; type=string; label=Path to template layouts (FE)
        layoutRootPath = EXT:dzbankuser/Resources/Private/Layouts/
    }
    persistence {
        # cat=plugin.tx_dzbankuser_subscriber//a; type=string; label=Default storage PID
        storagePid =
    }
}
