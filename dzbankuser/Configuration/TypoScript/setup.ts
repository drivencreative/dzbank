plugin.tx_dzbankuser_subscriber {
    view {
        templateRootPaths.0 = EXT:dzbankuser/Resources/Private/Templates/
        templateRootPaths.1 = {$plugin.tx_dzbankuser_subscriber.view.templateRootPath}
        partialRootPaths.0 = EXT:dzbankuser/Resources/Private/Partials/
        partialRootPaths.1 = {$plugin.tx_dzbankuser_subscriber.view.partialRootPath}
        layoutRootPaths.0 = EXT:dzbankuser/Resources/Private/Layouts/
        layoutRootPaths.1 = {$plugin.tx_dzbankuser_subscriber.view.layoutRootPath}
    }
    persistence {
        #storagePid = {$plugin.tx_dzbankuser_subscriber.persistence.storagePid}
        #recursive = 1
    }
    features {
        #skipDefaultArguments = 1
        # if set to 1, the enable fields are ignored in BE context
        ignoreAllEnableFieldsInBe = 0
        # Should be on by default, but can be disabled if all action in the plugin are uncached
        requireCHashArgumentForActionArguments = 1
    }
    mvc {
        #callDefaultActionIfActionCantBeResolved = 1
    }
    settings {
        group {
            consultant = 1
            user = 2
        }
        pid {
            users = 5
            protected = 6
        }
        login = 4
        sender = pcolovic@peak-sourcing.com
        subject = User created

    }
}

page.includeJS {
    jquery_validate = EXT:dzbankuser/Resources/Public/Js/jquery-validation/jquery.validate.js
    form = EXT:dzbankuser/Resources/Public/Js/form.js
}