$(document).ready(function () {
    validate('form.validate');
    function validate(el) {
        $(el).validate({
            errorElement: 'span',
            errorClass: 'help-block',
            highlight: function(element) {
                $(element).parents('.form-group').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).parents('.form-group').removeClass('has-error');
            }
        });
    }
});